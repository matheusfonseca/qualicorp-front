## Quali-front

Projeto front-end em vue.js para o teste. Aqui tento mostrar minha noção e habilidade em front-end. Destaco que tenho conhecimento em event-handling, lifecycle hooks, single file coomponents e etc. Uso a biblioteca [vuetify](https://vuetifyjs.com/en/) que gosto e acompanho faz bastante tempo. Facilita e acelera muito o desenvolvimento da UI.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
